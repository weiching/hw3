/*
 * @author weiching
 */
package hwwwwwwwwww3;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.json.JSONObject;


public class Server {
	public Vector output;
	public int result=0;
	@SuppressWarnings("resource")
	public void go() throws IOException {
		output = new Vector();
		try {
			int listenPort = 5000;
			ServerSocket serverSocket ;
			serverSocket = new ServerSocket(listenPort);
			Socket clientSocket;


			System.out.println("wait to client....");
			while(true){
				clientSocket = serverSocket.accept();
				PrintStream writer =  new PrintStream(clientSocket.getOutputStream());  //取得Socket的輸出資料流
				System.out.println(writer); 
				output.add(writer);
				new Thread(new Servergive(clientSocket)).start();;
			}
		} catch (Exception e) {
			System.out.println("some error:" + e.getMessage());}

	}



	public static void main(String[] args) throws IOException{ 
		new Server().go();;


	}


	class Servergive implements Runnable{
		public Socket clientSocket;
		public Servergive(Socket incommingsocket) {
			clientSocket = incommingsocket;

		}
		public void run(){


			try {
				DataInputStream in;
				PrintStream out;
				in = new DataInputStream(clientSocket.getInputStream());
				out = new PrintStream(clientSocket.getOutputStream());
				String str1,outstring,str2;
				int getint;
				while (true) { 
					@SuppressWarnings("deprecation")
					String jsonObjectinString=in.readUTF();
					System.out.println("the client comes message :" + jsonObjectinString);
					JSONObject jsonObjectin = new JSONObject(jsonObjectinString);				
					//int readInt= in.readInt();				
					str1= jsonObjectin.get("number").toString();
					str2= jsonObjectin.get("operators").toString();
					getint=Integer.valueOf(str1);
					if(str2.equals("add")){
						result=result+getint;
						Map map = new HashMap();
						map.put("result",result );
						JSONObject jsonObjectout = new JSONObject(map);
						outstring=jsonObjectout.toString();
						System.out.println("send message :" + outstring);
						tellApiece(outstring);
					}
					if(str2.equals("subtract")){
						result=result-getint;
						Map map = new HashMap();
						map.put("result",result );
						JSONObject jsonObjectout = new JSONObject(map);
						outstring=jsonObjectout.toString();
						System.out.println("send message :" + outstring);
						tellApiece(outstring);	
					}


				}



			} catch (IOException e) {
				System.out.println("some error:" + e.getMessage());
			}	
		}
		public void tellApiece(String message){
			//產生iterator可以存取集合內的元素資料    
			Iterator it = output.iterator();
			//向下讀取元件   
			while(it.hasNext()){          
				try{
					//取集合內資料
					PrintStream out = (PrintStream) it.next();  
					//印出
					out.println(message+"\n"); 
					//刷新該串流的緩衝。
					out.flush();           
				}
				catch(Exception ex){
					System.out.println("連接失敗Process");
				}
			}
		}

	}
}
